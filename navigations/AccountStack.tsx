import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import Account from '../screen/account/Account'
import Login from '../screen/account/Login'
import Register from '../screen/account/Register'

const Stack = createStackNavigator()


const AccountStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="account" component={Account} options={{ title: "Account" }} />
            <Stack.Screen name="login" component={Login} options={{ title: "Login" }} />
            <Stack.Screen name="register" component={Register} options={{ title: "Register" }} />
            {/* <Stack.Screen name="add-restaurant" component={AddRestaurant} options={{ title: "Crear Restaurante" }} />
            <Stack.Screen name="restaurant" component={Restaurant} /> */}
        </Stack.Navigator>
    )
}

export default AccountStack
