import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import Favorite from '../screen/Favorite'

const Stack = createStackNavigator()

const FavoritesStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="favorites" component={Favorite} options={{ title: "Favoritos" }} />
        </Stack.Navigator>
    )
}

export default FavoritesStack
