import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import Movies from '../screen/movies/Movies'


const Stack = createStackNavigator()


const MoviesStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="movies" component={Movies} options={{ title: "Movies" }} />
            {/* <Stack.Screen name="add-restaurant" component={AddRestaurant} options={{ title: "Crear Restaurante" }} />
            <Stack.Screen name="restaurant" component={Restaurant} /> */}
        </Stack.Navigator>
    )
}

export default MoviesStack
