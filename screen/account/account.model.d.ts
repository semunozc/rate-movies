export interface aspUsersDTO {
    id: number;
    Name: string;
    LastName: string;
    Token: string;
    Expiracion: Date;
}

export interface createAspUsersDTO {
    name: string;
    lastname: string;
    email: string;
    password: string;
    confirmPassword: string;
}


export interface modalProps {
    isVisible: boolean;
    text: string; 
}