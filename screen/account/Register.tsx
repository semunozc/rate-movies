import axios from "axios";
import React, { useState } from "react";
import { View, StyleSheet, Image } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import RegisterForm from "../../components/account/RegisterForm";
import { urlAccount, urlGeneros } from "../../utils/endpoints";
import { createAspUsersDTO } from "./account.model";

const Register = () => {
  const [errores, setErrores] = useState<string[]>([]);
//   const onChange = (e, type) => {
  //     setFormData({ ...formData, [type]: e.nativeEvent.text });
  //   };

  //   const doRegisterUser = async () => {
  //     if (!validateData()) {
  //       return;
  //     }
  //     setLoading(true)
  //     const result = await registerUser(formData.email, formData.password);
  //     setLoading(false)
  //     if (!result.statusRsponse) {
  //         setErrorEmail(result.error)
  //         return
  //     }
  //     navigation.navigate("account")
  //   };

  //   const validateData = () => {
  //     setErrorConfirm("");
  //     setErrorEmail("");
  //     setErrorPasword("");
  //     let isValid = true;

  //     if (!validateEmail(formData.email)) {
  //       setErrorEmail("Debes ingresar un email valido.");
  //       isValid = false;
  //     }

  //     if (size(formData.password) < 6) {
  //       setErrorPasword("Debes ingres una contrasena mayor a 5 caracteres");
  //       isValid = false;
  //     }

  //     if (size(formData.confirm) < 6) {
  //       setErrorConfirm("Debes ingresar una confirmacion mayor de 5 caracteres");
  //       isValid = false;
  //     }

  //     if (formData.password !== formData.confirm) {
  //       setErrorPasword("La contraseña y la confirmacion no son iguales");
  //       setErrorConfirm("La contraseña y la confirmacion no son iguales");
  //       isValid = false;
  //     }
  //     return isValid;
  //   };

  const create = async (genero: createAspUsersDTO) => {
    try {
      console.log(genero);
      //await axios.post(urlAccount, genero);
      //history.push('/generos');
    } catch (error) {
      setErrores(error.response.data);
    }
  };

  return (
    <KeyboardAwareScrollView>
      <Image
        source={require("../../assets/movie_logo.png")}
        resizeMode="contain"
        style={styles.image}
      />

      <RegisterForm
        modelo={{
          name: "",
          lastname: "",
          email: "",
          password: "",
          confirmPassword: "",
        }}
        onSubmit={async (valores) => {
          await create(valores);
        }}
      />
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  image: {
    height: 150,
    width: "100%",
    marginBottom: 20,
  },
});

export default Register;
