import * as SQLite from "expo-sqlite";
import { aspUsersDTO } from "../screen/account/account.model";

let db = SQLite.openDatabase("LocalDb.db");

export const createTablats = () => {
  db.transaction((tx) => {
    //tx.executeSql("DROP TABLE IF EXISTS AspUsers", []);
    tx.executeSql(
      "create table if not exists AspUsers (id integer primary key not null, Name text, LastName text, Token text, Expiracion text);"
    );
    //     tx.executeSql(`insert into AspUsers (LastName, Name, Token, Expiracion)
//     values ('Jairo','Munoz' ,'ASDASDSADASDASDASDSADASDASDASDSADASDASDASDASDASDASDSAD', '1/1/1')`);
  });
};

export const getCurrentUser = () => new Promise((resolve, reject) => {
    try {
      db.transaction((tx) => {
        tx.executeSql("SELECT * FROM AspUsers;", [], (tx, results) => {
          if (results.rows.length == 0) {
            return;
          }
          const { rows } = results;
          let users = {
            id: rows.item(0).id,
            Name: rows.item(0).Name,
            LastName: rows.item(0).LastName,
            Expiracion: rows.item(0).Expiracion,
            Token: rows.item(0).Token,
          };
          resolve(users);
        });
      });
    } catch (error) {
      reject(error);
    }
  });

// export function EditarEntidad<T>() {
//   db.transaction((tx) => {
//     tx.executeSql(`insert into AspUsers (LastName, Name, Token, Expiracion)
//     values ('Jairo','Munoz' ,'ASDASDSADASDASDASDSADASDASDASDSADASDASDASDASDASDASDSAD', '1/1/1')`);
//     tx.executeSql("select * from AspUsers", [], (_, { rows }) =>
//     mapping(rows)
//     );
//   });
//};

// const mapping = (user: aspUsersDTO) => {
//   return {
//       nombre: actor.nombre,
//       fotoURL: actor.foto,
//       biografia: actor.biografia,
//       fechaNacimiento: new Date(actor.fechaNacimiento)
//   }
// }
