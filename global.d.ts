import { StringSchema } from 'yup'

declare module 'yup' {
    class StringSchema {
        validateEmail (): this;
        validateSize (): this;
    }
}
