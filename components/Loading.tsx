import React from "react";
import { View, Text, StyleSheet, ActivityIndicator } from "react-native";
import { Overlay } from "react-native-elements";
import { modalProps } from "../screen/account/account.model";

const Loading = (modalProps: modalProps) => {
  return (
    <Overlay isVisible={modalProps.isVisible} overlayStyle={styles.overlay}>
      <View style={styles.view}>
        <ActivityIndicator size="large" color="#ec0c54" />
        {modalProps.text && <Text style={styles.text}>{modalProps.text}</Text>}
      </View>
    </Overlay>
  );
};
const styles = StyleSheet.create({
  overlay: {
    height: 100,
    width: 200,
    backgroundColor: "#fff",
    borderColor: "#ec447c",
    borderWidth: 2,
    borderRadius: 10,
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    color: "#ec0c54",
    marginTop: 10,
  },
});

export default Loading;
